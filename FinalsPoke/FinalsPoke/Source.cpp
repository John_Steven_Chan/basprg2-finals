#include <iostream>
#include <string>
#include "pokemons.h"
#include "time.h"
#include <vector>

using namespace std;

int main()
{
	srand(time(NULL));
	vector<Pokemon*> Bag;
	vector<Pokemon*> wildpokemons;

	Pokemon *caterpie = new Pokemon("Caterpie", 21, 21, 1, 9, 0, 26);
	wildpokemons.push_back(caterpie);
	Pokemon *weedle = new Pokemon("Weedle", 26, 26, 2, 11, 0, 29);
	wildpokemons.push_back(weedle);
	Pokemon *pidgey = new Pokemon("Pidgey", 25, 25, 2, 11, 0, 28);
	wildpokemons.push_back(pidgey);
	Pokemon *rattata = new Pokemon("Rattata", 29, 29, 3, 12, 0, 31);
	wildpokemons.push_back(rattata);
	Pokemon *spearow = new Pokemon("Spearow", 32, 32, 4, 13, 0, 33);
	wildpokemons.push_back(spearow);
	Pokemon *ekans = new Pokemon("Ekans", 35, 35, 5, 15, 0, 35);
	wildpokemons.push_back(ekans);
	Pokemon *pikachu = new Pokemon("Pikachu", 39, 39, 6, 16, 0, 41);
	wildpokemons.push_back(pikachu);
	Pokemon *sandshrew = new Pokemon("Sandshrew", 41, 41, 7, 14, 0, 41);
	wildpokemons.push_back(sandshrew);
	Pokemon *nidoranmale = new Pokemon("Nidoran (male)", 43, 43, 8, 16, 0, 43);
	wildpokemons.push_back(nidoranmale);
	Pokemon *nidoranfemale = new Pokemon("Nidoran (female)", 43, 43, 8, 16, 0, 43);
	wildpokemons.push_back(nidoranfemale);
	Pokemon *clefairy = new Pokemon("Clefairy", 39, 39, 7, 13, 0, 40);
	wildpokemons.push_back(clefairy);
	Pokemon *vulpix = new Pokemon("Vulpix", 41, 41, 8, 15, 0, 42);
	wildpokemons.push_back(vulpix);
	Pokemon *venemoth = new Pokemon("Venemoth", 50, 50, 10, 19, 0, 49);
	wildpokemons.push_back(venemoth);
	Pokemon *diglett = new Pokemon("Diglett", 26, 26, 5, 11, 0, 33);
	wildpokemons.push_back(diglett);
	Pokemon *zubat = new Pokemon("Zubat", 24, 24, 4, 10, 0, 30);
	wildpokemons.push_back(zubat);

	bool wildarea = false;
	int x = 0;
	int y = 0;
	int chancetofight=0;
	int whatpokemon = 0;
	string playername;
	string decision;

	cout << "Hi, I'm Professor Oak, What's your name? " << endl;
	cin >> playername;
	system("cls");
	cout << "Hi " << playername << " please choose one of the three pokemons!" << endl;
	cout << "[a] Charmander" << endl;
	cout << "[b] Squirtle" << endl;
	cout << "[c] Bulbasour" << endl;
	cin >> decision;

	if (decision == "a")
	{
		Pokemon *charmander = new Pokemon("Charmander", 39, 39, 5, 12, 0, 35);
		charmander->displaypokemon();
		Bag.push_back(charmander);
	}
	else if (decision == "b")
	{
		Pokemon *squirtle = new Pokemon("Squirtle", 37, 37, 5, 12, 0, 35);
		squirtle->displaypokemon();
		Bag.push_back(squirtle);
	}
	else if (decision == "c")
	{
		Pokemon *bulbasour = new Pokemon("Bulbasour", 38, 38, 5, 12, 0, 35);
		bulbasour->displaypokemon();
		Bag.push_back(bulbasour);
	}
	system("pause");
	system("cls");

	cout << "your journey begins now!" << endl;
	system("pause");
	system("cls");

	while (true)
	{
		string todo;
		if (wildarea == false)
		{
			cout << "you are now at position (" << x << ", " << y << ")" << endl;
			cout << "What would you like to do? " << endl;
			cout << "[1] Move" << endl;
			cout << "[2] Pokemon/s" << endl;
			cout << "[3] Pokemon center" << endl;
			cin >> todo;
			system("cls");
		}
		else if (wildarea == true)
		{
			cout << "you are now at position (" << x << ", " << y << ")" << endl;
			cout << "What would you like to do? " << endl;
			cout << "[1] Move" << endl;
			cout << "[2] Pokemon/s" << endl;
			cin >> todo;
			system("cls");
		}

		if (todo == "1")
		{
			string movedirection;
			cout << "where do you want to move?" << endl;
			cout << "W - up" << endl;
			cout << "S - down" << endl;
			cout << "A - left" << endl;
			cout << "D - right" << endl;
			cin >> movedirection;
			if (movedirection == "w")
			{
				y++;
			}
			else if (movedirection == "s")
			{
				y--;
			}
			else if (movedirection == "a")
			{
				x--;
			}
			else if (movedirection == "d")
			{
				x++;
			}
			system("cls");
		}
		else if (todo == "2")
		{
			for (int i = 0; i < Bag.size(); i++)
			{
				Bag[i]->displaypokemon();
				cout << endl;
			}
			system("pause");
			system("cls");
		}
		else if (todo == "3" && wildarea == false)
		{
			string yesorno;
			cout << "Hi welcome to the Pokemon Center would you like me to heal your pokemons? (y/n)" << endl;
			cin >> yesorno;
			if (yesorno == "y")
			{
				for (int i = 0; i < Bag.size(); i++)
				{
					Bag[i]->healpokemons();
					Bag[i]->displaypokemon();
				}
				cout << "farewell and good luck on your journey" << endl;
				system("pause");
				system("cls");
			}
			else if (yesorno == "n")
			{
				cout << "farewell and good luck on your journey" << endl;
			}
		}
		if (x >= -2 && x <= 2 && y >= -2 && y <= 2)
		{
			cout << "you are in Pallet town" << endl;
			wildarea = false;
		}
		else if (x > 2 && x <= 4 && y > 2 && y <= 4)
		{
			cout << "you are in Route 1 (wild location)" << endl;
			wildarea = true;
		}
		else if (x > -2 && x <= -4 && y > 2 && y <= 4)
		{
			cout << "you are in Route 2 (wild location)" << endl;
			wildarea = true;
		}
		else if (x > -2 && x <= -4 && y > -2 && y <= -4)
		{
			cout << "you are in Route 24 (wild location)" << endl;
			wildarea = true;
		}
		else if (x > 2 && x <= 4 && y > -2 && y <= -4)
		{
			cout << "you are in Mt. Moon (wild location)" << endl;
			wildarea = true;
		}
		else
		{
			cout << "you are in an unknown location" << endl;
			wildarea = true;
		}

		if (wildarea == true)
		{
			chancetofight = rand() % 100 + 1;
			if (chancetofight <= 80)
			{
				for (int i = 0; i < wildpokemons.size(); i++)
				{
					wildpokemons[i]->healpokemons();
				}

				whatpokemon = rand() % wildpokemons.size();
				cout << "A WILD POKEMON HAS APPEARED" << endl;
				system("pause");
				int currentpoke = 0;

				for (int i = 0; i < Bag.size(); i++)
				{
					if (Bag[i]->isdead())
					{
						currentpoke++;
					}
				}

				while (true)
				{
					Bag[currentpoke]->displaypokemon();
					cout << endl;
					wildpokemons[whatpokemon]->displaypokemon();
					string decisionencounter;
					cout << "what would you like to do?" << endl;
					cout << "[1] Battle" << endl;
					cout << "[2] Catch" << endl;
					cout << "[3] Flee" << endl;
					cin >> decisionencounter;

					int chancetoattack = rand() % 100 + 1;
					int chancetocatch = rand()% 100 + 1;

					if (decisionencounter == "1")
					{
						if (chancetoattack <= 80)
						{
							Bag[currentpoke]->attackingpoke(wildpokemons[whatpokemon]);
							system("pause");

							if (wildpokemons[whatpokemon]->isdead())
							{
								cout << endl;
								cout << "wild pokemon fainted" << endl;
								Bag[currentpoke]->gainingexp();
								Bag[currentpoke]->leveling();
								Bag[currentpoke]->displaypokemon();
								system("pause");
								system("cls");
								break;
							}

							wildpokemons[whatpokemon]->attackingpoke(Bag[currentpoke]);
							system("pause");
							system("cls");
							
							if (Bag[currentpoke]->isdead())
							{
								for (int i = 0; i < Bag.size(); i++)
								{
									if (Bag[i]->isdead())
									{
										currentpoke++;
									}
								}
							}
							if (currentpoke == Bag.size())
							{
								cout << "your pokemon/s have fainted, you will now return to Pallet town" << endl;
								for (int i = 0; i < Bag.size(); i++)
								{
									Bag[i]->healpokemons();
								}
								x = 0;
								y = 0;
								wildarea = false;
								system("pause");
								system("cls");
								break;
							}
							
						}
						else
						{
							cout << "you missed" << endl;
							system("pause");
							wildpokemons[whatpokemon]->attackingpoke(Bag[currentpoke]);
							system("pause");
							system("cls");
							if (Bag[currentpoke]->isdead())
							{
								for (int i = 0; i < Bag.size(); i++)
								{
									if (Bag[i]->isdead())
									{
										currentpoke++;
									}
								}
							}
							if (currentpoke == Bag.size())
							{
								cout << "your pokemon/s have fainted, you will now return to Pallet town" << endl;
								for (int i = 0; i < Bag.size(); i++)
								{
									Bag[i]->healpokemons();
								}
								x = 0;
								y = 0;
								wildarea = false;
								system("pause");
								system("cls");
								break;
							}
						}
					}
					else if (decisionencounter == "2")
					{
						if (Bag.size() < 6)
						{
							if (chancetocatch <= 30)
							{
								cout << "you have successfully caught the pokemon" << endl;
								Bag.push_back(wildpokemons[whatpokemon]);
								wildpokemons.erase(wildpokemons.begin() + whatpokemon);
								system("pause");
								system("cls");
								break;
							}
							else
							{
								cout << "the pokemon got out of the pokeball" << endl;
								system("pause");
								system("cls");
							}
						}
						else
						{
							cout << "you don't have space to catch anymore" << endl;
							system("pause");
							system("cls");
						}
					}
					else if (decisionencounter == "3")
					{
						cout << "you have successfully escaped" << endl;
						system("pause");
						system("cls");
						break;
					}
					system("cls");
				}
			}
		}
	}
}