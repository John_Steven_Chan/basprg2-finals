#pragma once
#include <string>

using namespace std;

class Pokemon
{
	public:
		Pokemon();
		Pokemon(string name, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel);
		void displaypokemon();
		void attackingpoke(Pokemon* enemy);
		void healpokemons();
		bool isdead();
		void gainingexp();
		void leveling();

	private:
		string name;
		int baseHp;
		int hp;
		int level;
		int baseDamage;
		int exp;
		int expToNextLevel;
};
