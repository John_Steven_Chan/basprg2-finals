#include "pokemons.h"
#include <iostream>

using namespace std;

Pokemon::Pokemon()
{
	name = "";
	baseHp = 0;
	hp = 0;
	level = 0;
	baseDamage = 0;
	exp = 0;
	expToNextLevel = 0;
}

Pokemon::Pokemon(string name, int baseHp, int hp, int level,int baseDamage, int exp, int expToNextLevel)
{
	this->name = name;
	this->baseHp = baseHp;
	this->hp = hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
}

void Pokemon::displaypokemon()
{
	cout << "Pokemon : " << this->name << endl;
	cout << "Health : " << this->hp << endl;
	cout << "Level : " << this->level << endl;
	cout << "Damage : " << this->baseDamage << endl;
	cout << "Exp : " << this->exp << " / " << this->expToNextLevel << endl;
}

void Pokemon::attackingpoke(Pokemon* enemy)
{
	cout << name << " attacked " << enemy->name << " and dealt "<< baseDamage <<" damage"<< endl;
	enemy->hp -= this->baseDamage;
	cout << enemy->name << " has " << enemy->hp << " health left." << endl;
	cout << endl;
}

void Pokemon::healpokemons()
{
	this->hp = this->baseHp;
}

bool Pokemon::isdead()
{
	return hp <= 0;
}

void Pokemon::gainingexp()
{
	this->exp = this->exp + (this->expToNextLevel*0.2);
}

void Pokemon::leveling()
{
	if (this->exp == this->expToNextLevel)
	{
		cout << "LEVEL UP!" << endl;
		this->exp = 0;
		this->baseHp = this->baseHp + (this->baseHp*0.15);
		this->hp = this->baseHp;
		this->baseDamage = this->baseDamage + (this->baseDamage*0.1);
		this->expToNextLevel = this->expToNextLevel + (this->expToNextLevel*0.2);
		this->level++;
	}
}
